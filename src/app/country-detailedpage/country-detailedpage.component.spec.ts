import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryDetailedpageComponent } from './country-detailedpage.component';

describe('CountryDetailedpageComponent', () => {
  let component: CountryDetailedpageComponent;
  let fixture: ComponentFixture<CountryDetailedpageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryDetailedpageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryDetailedpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
