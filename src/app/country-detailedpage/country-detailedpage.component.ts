import { Component, OnInit } from '@angular/core';
import { PostService } from '../countriesservice/post.service';
import { countries } from '../Countries';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-country-detailedpage',
  templateUrl: './country-detailedpage.component.html',
  styleUrls: ['./country-detailedpage.component.css']
})
export class CountryDetailedpageComponent implements OnInit {
 
  public countries : countries[]=[];
  

  constructor(public service:PostService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    const name = (this.route.snapshot.paramMap.get("name"))
    this.service.getPosts()
        .subscribe(response => {
          this.countries = response.filter((country)=>{
            return country.name.common === name
          })
        });
  }
  }


