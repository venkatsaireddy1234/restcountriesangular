import { Component, OnInit } from '@angular/core';
import { PostService } from '../countriesservice/post.service';
import { countries } from '../Countries';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})


export class MainComponent implements OnInit {

  public countries : countries[]=[];

  constructor(public service:PostService) { }

  ngOnInit(): void {
    this.service.getPosts()
        .subscribe(response => {
          this.countries = response;
        });
  }
}
