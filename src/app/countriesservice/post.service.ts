import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { countries } from '../Countries';
import { Observable, of } from 'rxjs';
  
@Injectable({
  providedIn: 'root'
})
export class PostService {
  private apiurl = 'https://restcountries.com/v3.1/all';
   
  constructor(private httpClient: HttpClient) { }
  
  getPosts():Observable<countries[]>{
    return this.httpClient.get<countries[]>(this.apiurl);
  }
  
}