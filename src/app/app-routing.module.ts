import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CountryDetailedpageComponent } from './country-detailedpage/country-detailedpage.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {path: "", component:MainComponent},
  {path : ":name", component:CountryDetailedpageComponent}
];

  @NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents= [CountryDetailedpageComponent,MainComponent]
