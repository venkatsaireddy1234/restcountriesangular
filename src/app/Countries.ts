export interface countries{
    name:{
      common:string,
      nativeName:{[key:string]:string}
      official:string
      
    },
    flags:{
      png:string
    },
    population:number;
    region:string;
    capital:string;
    subregion:string;
    tld:[string],
    currencies:[number];
    languages:string;
    borders:[string]
  
  }